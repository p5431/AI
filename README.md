# Welcome to Greenhealts AI API!

In this file we will explain the steps you have to follow to make a succesfull model which will give an accurate prediction of in what stage the plant is.


# Files

First we will go over each file and the use of them.

## App .py

The file explorer is accessible using the button in left corner of the navigation bar. You can create a new file by clicking the **New file** button in the file explorer. You can also create folders by clicking the **New folder** button.

## Project_4_AI_Deel .ipynb

All your files and folders are presented as a tree in the file explorer. You can switch from one to another by clicking a file in the tree.

## Requirements.txt

You can rename the current file by clicking the file name in the navigation bar or by clicking the **Rename** button in the file explorer.


# Steps of making a successfull model
You can open a file from **Google Drive**, **Dropbox** or **GitHub** by opening the **Synchronize** sub-menu and clicking **Open from**. Once opened in the workspace, any modification in the file will be automatically synced.

## Step 1: Download the photos

You can save any file of the workspace to **Google Drive**, **Dropbox** or **GitHub** by opening the **Synchronize** sub-menu and clicking **Save on**. Even if a file in the workspace is already synced, you can save it to another location. StackEdit can sync one file with multiple locations and accounts.

## Step 2: Import all the necessary libraries
Once your file is linked to a synchronized location, StackEdit will periodically synchronize it by downloading/uploading any modification. A merge will be performed if necessary and conflicts will be resolved.

If you just have modified your file and you want to force syncing, click the **Synchronize now** button in the navigation bar.

> **Note:** The **Synchronize now** button is disabled if you have no file to synchronize.

## Step 3: Execute the code

Since one file can be synced with multiple locations, you can list and manage synchronized locations by clicking **File synchronization** in the **Synchronize** sub-menu. This allows you to list and remove synchronized locations that are linked to your file.


# Run the py file

Publishing in StackEdit makes it simple for you to publish online your files. Once you're happy with a file, you can publish it to different hosting platforms like **Blogger**, **Dropbox**, **Gist**, **GitHub**, **Google Drive**, **WordPress** and **Zendesk**. With [Handlebars templates](http://handlebarsjs.com/), you have full control over what you export.

> **ProTip:** Make sure you have installed all the necesarry libraries.




